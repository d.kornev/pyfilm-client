// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueResource from 'vue-resource'

import VueMoment from 'vue-moment'

import router from './router'
import { store } from './store/store'

Vue.use(VueResource)
Vue.use(VueMoment)

if (process.env.NODE_ENV === 'development') {
  Vue.http.options.root = 'http://127.0.0.1:5000'
  window.staticUrl = 'http://127.0.0.1:5001'
} else {
  Vue.http.options.root = 'https://api.blogexample.tk'
  window.staticUrl = 'https://static.blogexample.tk'
}
if (store.state.auth.token) {
  Vue.http.headers.common['Authorization'] = `Bearer ${store.state.auth.token}`
}

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
