import Vue from 'vue'
import Router from 'vue-router'
import Hello from 'components/Hello'

import PostList from 'components/blog/PostList'
import PostCreate from 'components/blog/PostCreate'
import PostDetail from 'components/blog/PostDetail'
import Login from 'components/auth/Login'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'blog',
      component: PostList
    },
    {
      path: '/blog/:action',
      name: 'blog-post-create',
      component: PostCreate
    },
    {
      path: '/blog/:date/:slug/:action',
      name: 'blog-post-edit',
      component: PostCreate
    },
    {
      path: '/blog/:date/:slug',
      name: 'blog-post-detail',
      component: PostDetail
    },
    {
      path: '/about',
      name: 'about',
      component: Hello
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    }
  ],
  mode: 'history',
  scrollBehavior (to, from, savedPosition) {
    if (to.hash) {
      return {selector: to.hash}
    }
  }
})
