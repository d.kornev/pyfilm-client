import Vue from 'vue'
import Vuex from 'vuex'

import { auth } from './modules/auth'
import { blog } from './modules/blog'
import { upload } from './modules/upload'
import { notify } from './modules/notify'

Vue.use(Vuex)

export const store = new Vuex.Store({
  modules: {
    auth,
    blog,
    upload,
    notify
  }
})
