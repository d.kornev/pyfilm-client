export const blog = {
  state: {
    posts: [],
    comments: []
  },
  mutations: {
    deletePost (state, index) {
      state.posts.splice(index, 1)
    },
    deleteComment (state, index) {
      state.comments.splice(index, 1)
    },
    setComments (state, comments) {
      state.comments = comments
    },
    addComment (state, comment) {
      state.comments.push(comment)
    }
  }
}
