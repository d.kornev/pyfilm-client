import Vue from 'vue'

export const auth = {
  state: {
    token: localStorage.getItem('token')
  },
  mutations: {
    setToken (state, token) {
      localStorage.setItem('token', token)
      state.token = token
      Vue.http.headers.common['Authorization'] = `Bearer ${token}`
    },
    removeToken (state) {
      localStorage.removeItem('token')
      state.token = null
      Vue.http.headers.common['Authorization'] = null
    }
  },
  getters: {
    isAuthenticated: (state) => {
      return !!state.token
    }
  }
}
