export const upload = {
  state: {
    files: []
  },
  mutations: {
    setFiles (state, files) {
      state.files = files
    }
  }
}
