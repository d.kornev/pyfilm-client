export const notify = {
  state: {
    alerts: {
      'success': [],
      'info': [],
      'warning': [],
      'danger': []
    }
  },
  getters: {
    getAlerts (state) {
      return state.alerts
    }
  },
  mutations: {
    addAlert (state, payload) {
      state.alerts[payload.type].push(payload.message)
    },
    removeAlert (state, payload) {
      state.alerts[payload.type].splice(payload.index, 1)
    },
    cleanAlerts (state) {
      state.alerts = {
        'success': [],
        'info': [],
        'warning': [],
        'danger': []
      }
    }
  }
}
